package com.example.thtec.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.example.thtec.R;
import com.example.thtec.adapters.StudentAdapter;
import com.example.thtec.database.model.Student;
import com.example.thtec.utils.Space;
import com.example.thtec.viewmodel.StudentListViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    StudentListViewModel mStudentListViewModel;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Initialize floating action button
        fab = (FloatingActionButton) findViewById(R.id.fab);
        //show add notes dialogue
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        // bind recyclerview to object
        RecyclerView rvStudent = findViewById(R.id.recyclerViewNotes);
        // set layout manager
        rvStudent.setLayoutManager(new LinearLayoutManager(this));
        // create new notes adapter
        final StudentAdapter studentAdapter = new StudentAdapter();
        // set adapter to recyclerview
        rvStudent.setAdapter(studentAdapter);
        // add decoration to recyclerview
        rvStudent.addItemDecoration(new Space(20));
        // get ViewModel of this activity using ViewModelProviders
        mStudentListViewModel = ViewModelProviders.of(this).get(StudentListViewModel.class);
        // observe for notes data changes
        mStudentListViewModel.getAllStudents().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(@Nullable List<Student> students) {
                //add notes to adapter
                studentAdapter.addStudents(students);
            }
        });

    }

    public void showDialog() {
        fab.hide();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_student_dialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final EditText etNewStudentName = dialog.findViewById(R.id.etNewStudentName);
        final EditText etNewStudentGrade = dialog.findViewById(R.id.etNewStudentGrade);
        final EditText etNewCalifSpanish = dialog.findViewById(R.id.etNewCalifSpanish);
        final EditText etNewCalifMath = dialog.findViewById(R.id.etNewCalifMath);
        final EditText etNewCalifHistory = dialog.findViewById(R.id.etNewCalifHistory);

        TextView btAdd = dialog.findViewById(R.id.btAdd);
        TextView btCancel = dialog.findViewById(R.id.btCancel);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String studentName = etNewStudentName.getText().toString();
                String studentGrade = etNewStudentGrade.getText().toString();
                String califSpanish = etNewCalifSpanish.getText().toString();
                String califMath = etNewCalifMath.getText().toString();
                String califHistory = etNewCalifHistory.getText().toString();


                //add note
                mStudentListViewModel.addStudent(new Student(studentName, studentGrade, califSpanish, califMath, califHistory));
                fab.show();
                dialog.dismiss();
            }
        });
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                fab.show();
            }
        });

        dialog.show();

    }
}
