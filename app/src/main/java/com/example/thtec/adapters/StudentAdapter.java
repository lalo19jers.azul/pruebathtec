package com.example.thtec.adapters;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.thtec.R;
import com.example.thtec.database.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter {
    //Create list of notes
    List<Student> students = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Get layout inflater
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        //Inflate layout
        View row = inflater.inflate(R.layout.item_student, parent, false);
        //return notes holder and pass row inside
        return new StudentHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //Get current student
        Student currentStudent = students.get(position);
        //cast notes holder
        StudentHolder studentHolder = (StudentHolder) holder;
        //set title description and created at
       // studentHolder.tvStudentID.setText(currentStudent.getStudentID());
        studentHolder.tvStudentName.setText(currentStudent.getName());
        studentHolder.tvStudentGrade.setText(currentStudent.getGrade());
        studentHolder.tvCalifSpanish.setText(currentStudent.getCalifSpanish());
        studentHolder.tvCalifMath.setText(currentStudent.getCalifMath());
        studentHolder.tvCalifHistory.setText(currentStudent.getCalifHistory());


    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class StudentHolder extends RecyclerView.ViewHolder {
        TextView tvStudentID, tvStudentName, tvStudentGrade, tvCalifSpanish, tvCalifMath, tvCalifHistory;


        public StudentHolder(View itemView) {
            super(itemView);
            tvStudentID = itemView.findViewById(R.id.tvStudentID);
            tvStudentName = itemView.findViewById(R.id.tvStudentName);
            tvStudentGrade = itemView.findViewById(R.id.tvStudentGrade);
            tvCalifSpanish = itemView.findViewById(R.id.tvCalifSpanish);
            tvCalifMath = itemView.findViewById(R.id.tvCalifMath);
            tvCalifHistory = itemView.findViewById(R.id.tvCalifHistory);

        }
    }

    public void addStudents(List<Student> students) {
        this.students = students;
        notifyDataSetChanged();
    }
}