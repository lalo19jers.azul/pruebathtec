package com.example.thtec.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.thtec.database.dao.StudentDAO;
import com.example.thtec.database.model.Student;

// Room database class
@Database(entities = Student.class, version = 1, exportSchema = false)
public abstract class SchoolDatabase extends RoomDatabase {
    //define static instance
    private static SchoolDatabase mInstance;

    //method to get room database
    public static SchoolDatabase getDatabase(Context context) {

        if (mInstance == null)
            mInstance = Room.databaseBuilder(context.getApplicationContext(),
                    SchoolDatabase.class, "school_db")
                    .build();

        return mInstance;
    }

    //method to remove instance
    public static void closeDatabase() {
        mInstance = null;
    }

    //define school dao ( data access object )
    public abstract StudentDAO studentDAO();


}