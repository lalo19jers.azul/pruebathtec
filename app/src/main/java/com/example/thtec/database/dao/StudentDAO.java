package com.example.thtec.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.thtec.database.model.Student;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

//note dao(data access object)
@Dao

public interface StudentDAO {
    // Dao method to get all notes
    @Query("SELECT * FROM Student")
    LiveData<List<Student>> getAllStudents();

    // Dao method to insert note
    @Insert(onConflict = REPLACE)
    void insertStudent(Student student);

    // Dao method to delete note
    @Delete
    void deleteStudent(Student student);
}
