package com.example.thtec.database.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity
public class Student {
    // room database entity primary key
    @PrimaryKey(autoGenerate = true)
    public int studentID;
    private String name;
    private String grade;
    private String califSpanish;
    private String califMath;
    private String califHistory;


    public Student( String name, String grade, String califSpanish, String califMath, String califHistory) {

        this.name = name;
        this.grade = grade;
        this.califSpanish = califSpanish;
        this.califMath = califMath;
        this.califHistory = califHistory;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getCalifSpanish() {
        return califSpanish;
    }

    public void setCalifSpanish(String califSpanish) {
        this.califSpanish = califSpanish;
    }

    public String getCalifMath() {
        return califMath;
    }

    public void setCalifMath(String califMath) {
        this.califMath = califMath;
    }

    public String getCalifHistory() {
        return califHistory;
    }

    public void setCalifHistory(String califHistory) {
        this.califHistory = califHistory;
    }
}
