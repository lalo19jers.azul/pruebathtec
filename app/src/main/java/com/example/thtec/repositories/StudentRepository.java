package com.example.thtec.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.thtec.database.SchoolDatabase;
import com.example.thtec.database.dao.StudentDAO;
import com.example.thtec.database.model.Student;

import java.util.List;

//Student repository
public class StudentRepository {
    //Live Data of List of all notes
    private LiveData<List<Student>> mAllStudents;
    //Define Notes Dao
    StudentDAO mStudentDAO;

    public StudentRepository(@NonNull Application application) {
        SchoolDatabase schoolDatabase = SchoolDatabase.getDatabase(application);
        //init Notes Dao
        mStudentDAO = schoolDatabase.studentDAO();
        //get all notes
        mAllStudents = mStudentDAO.getAllStudents();
    }
    //method to get all notes
    public LiveData<List<Student>> getmAllStudents() {
        return mAllStudents;
    }

    //method to add Student
    public void addStudent(Student student) {
        new AddStudent().execute(student);
    }

    //Async task to add student
    public class AddStudent extends AsyncTask<Student, Void, Void> {
        @Override
        protected Void doInBackground(Student... students) {
            mStudentDAO.insertStudent(students[0]);
            return null;
        }
    }
}