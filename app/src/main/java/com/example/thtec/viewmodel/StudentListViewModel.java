package com.example.thtec.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.thtec.database.model.Student;
import com.example.thtec.repositories.StudentRepository;

import java.util.List;

public class StudentListViewModel extends AndroidViewModel {
    private LiveData<List<Student>> mAllStudents;
    StudentRepository mStudentRepository;

    public StudentListViewModel(@NonNull Application application) {
        super(application);
        mStudentRepository = new StudentRepository(application);
        mAllStudents = mStudentRepository.getmAllStudents();
    }

    public LiveData<List<Student>> getAllStudents() {
        return mAllStudents;
    }

    public void addStudent(Student student) {
        mStudentRepository.addStudent(student);
    }
}
